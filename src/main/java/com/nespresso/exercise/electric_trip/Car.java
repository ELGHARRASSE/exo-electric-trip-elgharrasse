package com.nespresso.exercise.electric_trip;

import com.nespresso.exercise.electric_trip.reporter.Reporter;

public class Car {
	
	private Battery battery;
	private int lowSpeedPerformance;
	private int highSpeedPerformance;
	private City city;
	
	public Car(int batterySize, int lowSpeedPerformance,
			int highSpeedPerformance, City city) {
		super();
		this.battery = new Battery(batterySize);
		this.lowSpeedPerformance = lowSpeedPerformance;
		this.highSpeedPerformance = highSpeedPerformance;
		this.city = city;
	}

	public void moveWithLowerCharge() {	
		city=city.moveWithCharge(lowSpeedPerformance, battery);
	}

	public String location(Reporter reporter) {
		return city.report(reporter);
	}

	public int charge() {
		return battery.charge();
	}

	public void moveWithHigherCharge() {
		city=city.moveWithCharge(highSpeedPerformance, battery);
	}
	
	
	
	

}
