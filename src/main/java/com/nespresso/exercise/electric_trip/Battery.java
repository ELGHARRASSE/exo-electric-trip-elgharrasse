package com.nespresso.exercise.electric_trip;

public class Battery {
	
	private double maxSize;
	private double size;

	public Battery(int maxSize) {
		super();
		this.maxSize = maxSize;
		this.size = maxSize;
	}


	public void reduce(int energy){
		this.size -= energy;
	}
	
	public int charge(){
		double charge = (double) ((size * 100) / maxSize);
		return (int)Math.round(charge);
	}


	public boolean canConsume(int energyConsumed) {
		return size>=energyConsumed;
	}

}
