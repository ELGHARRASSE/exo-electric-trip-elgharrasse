package com.nespresso.exercise.electric_trip.parser;

import java.util.HashMap;
import java.util.Map;

import com.nespresso.exercise.electric_trip.City;

public class DefaultCityParser implements CityParser {

	private static final String SEPARATOR = "-";

	public Map<String, City> parseCities(String trip) {
		String[] tripSplitted = trip.split(SEPARATOR);
		Map<String, City> cities = new HashMap<String, City>();
		City previousCity = null;
		String distance = null;
		for(int i=0; i<tripSplitted.length ; i+=2){
			String cityName = tripSplitted[i];
			if(i!=0)
			distance = tripSplitted[i-1];
			City city = new City(cityName);
			if(previousCity != null){
				previousCity.setNextCity(city, new Integer(distance));
			}
			cities.put(cityName, city);
			previousCity = city;
		}
		return cities;
	}



}
