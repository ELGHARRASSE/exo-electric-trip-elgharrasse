package com.nespresso.exercise.electric_trip.parser;

import java.util.Map;

import com.nespresso.exercise.electric_trip.City;

public interface CityParser {

	Map<String, City> parseCities(String trip);

}
