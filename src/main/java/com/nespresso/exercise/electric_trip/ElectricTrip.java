package com.nespresso.exercise.electric_trip;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.nespresso.exercise.electric_trip.parser.CityParser;
import com.nespresso.exercise.electric_trip.parser.DefaultCityParser;
import com.nespresso.exercise.electric_trip.reporter.DefaultReporter;
import com.nespresso.exercise.electric_trip.reporter.Reporter;

public class ElectricTrip {
	
	private List<Car> cars = new ArrayList<Car>(); 
	private Map<String, City> cities;
	
	public ElectricTrip(String trip) {
		CityParser cityParser = new DefaultCityParser();
		cities = cityParser.parseCities(trip);
	}

	public int startTripIn(String cityDesc, int batterySize,
			int lowSpeedPerformance, int highSpeedPerformance) {
		
		City city = cities.get(cityDesc);
		Car car = new Car(batterySize, lowSpeedPerformance, highSpeedPerformance, city);
		cars.add(car);
		return cars.size() - 1;
	}

	public void go(int participantId) {
		Car car = cars.get(participantId);
		car.moveWithLowerCharge();
	}

	public String locationOf(int participantId) {
		Reporter reporter = new DefaultReporter();
		Car car = cars.get(participantId);
		return car.location(reporter);
	
	}

	public String chargeOf(int participantId) {
		Reporter reporter = new DefaultReporter();
		Car car = cars.get(participantId);
		int charge = car.charge();
		return reporter.reportCharge(charge);
	}

	public void sprint(int participantId) {
		Car car = cars.get(participantId);
		car.moveWithHigherCharge();
	}

	public void charge(int id, int hoursOfCharge) {
		// TODO Auto-generated method stub
		
	}

}
