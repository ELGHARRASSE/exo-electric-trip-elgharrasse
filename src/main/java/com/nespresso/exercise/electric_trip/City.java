package com.nespresso.exercise.electric_trip;

import com.nespresso.exercise.electric_trip.reporter.Reporter;

public class City {

	public String name;
	private City nextCity;
	private int distance;

	public City(String name) {
		super();
		this.name = name;
	}

	public void setNextCity(City nextCity, int distance) {
		this.distance = distance;
		this.nextCity = nextCity;
	}

	private int energyConsumedForNextCity(int speedPerformance) {
		return this.distance / speedPerformance;
	}

	public City next() {
		return nextCity;
	}

	public City moveWithCharge(int lowSpeedPerformance, Battery battery) {
		int energyConsumed = energyConsumedForNextCity(lowSpeedPerformance);
		if(nextCity != null && battery.canConsume(energyConsumed)){
			battery.reduce(energyConsumed);
			return nextCity.moveWithCharge(lowSpeedPerformance, battery);
		}
		return this;
	}

	public String report(Reporter reporter) {
		return reporter.reportCity(this.name);
	}



	
	
}
