package com.nespresso.exercise.electric_trip.reporter;

public interface Reporter {

	String reportCharge(int charge);

	String reportCity(String name);

}
