package com.nespresso.exercise.electric_trip.reporter;

public class DefaultReporter implements Reporter {

	public String reportCharge(int charge) {
		return new String(charge+"%");
	}

	public String reportCity(String name) {
		return name;
	}

}
